﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CardGame
{
    partial class Form1 : Form
    {
        
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        private NetworkStream clientStream;


        //
        //GLOBAL VARIBELS
        //
        int compScore = 0;
        int userScore = 0;

        public void For()
        {
            InitializeComponent();
            Thread thread = new Thread(connection);
            thread.Start();
        }
        

        /**
         Connect the server to the client.
         */
        public void connection()
        {
            try
            {
                TcpClient client = new TcpClient();

                IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8820);

                client.Connect(serverEndPoint);

                NetworkStream clientStream = client.GetStream();
                this.clientStream = clientStream;
                byte[] bufferIn = new byte[4];              //Receive massage.
                int bytesRead = clientStream.Read(bufferIn, 0, 4);
                string input = new ASCIIEncoding().GetString(bufferIn);

                while (input != null)
                {
                    generatCards(input);
                }
            }

            catch (Exception e)
            {
                MessageBox.Show("Connection failed. start");
                Invoke((MethodInvoker)delegate { this.Close(); });
            }
        }

        /**
         Set 10 clickd cardes and 1 unclicked.
         */
        public void generatCards(string input)
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            for (int i = 0; i < 10; i++)    //Creates ten picturBoxs.
            {
                System.Windows.Forms.PictureBox currPic = new PictureBox();
                currPic.Location = new System.Drawing.Point(i+25, 0);
                currPic.Name = "picDynamic" + i;
                currPic.Image = ((System.Drawing.Image)(resources.GetObject("card back blue.png")));
                currPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
                currPic.Size = new System.Drawing.Size(25, 50);
                currPic.TabIndex = 0;
                currPic.TabStop = false;
                currPic.Click += delegate(object sender1, EventArgs e1)
                                    {
                                        string card = cardClicked();
                                        currPic.Image = ((System.Drawing.Image)(resources.GetObject(card)));
                                        whoWon(card, input);
                                    };
            }

            System.Windows.Forms.PictureBox Pic = new PictureBox();
            Pic.Image = ((System.Drawing.Image)(resources.GetObject("card back red.png")));
            Pic.Location = new System.Drawing.Point(129, 144);
            Pic.Name = "pictureBox1";
            Pic.Size = new System.Drawing.Size(25, 50);
            Pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            Pic.TabIndex = 0;
            Pic.TabStop = false;
        }

        /**
         Output: Random value name card.
        */
        public string cardClicked()
        {
            Random r = new Random();
            string[] kind = { "spades", "dimondes", "hearts", "clubs" };
            int rand1 = r.Next(0, 4);
            string[] number = { "ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "jack", "queen", "king" };
            int rand2 = r.Next(0, 13);           
            return (number[rand2] + "_of_" + kind[rand1] + ".png");
        }
        /**
         Checks who won this turn.
         */
        public void whoWon(string user, string computer)
        {
            string[] splitedU = user.Split('_');
            string[] splitedC = user.Split('_');
            //string[] kind = { "spades.png", "dimondes.png", "hearts.png", "clubs.png" };
            string[] number = { "ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "jack", "queen", "king" };
            if (splitedU[-1] == splitedC[-1])
            {
                int posU = Array.IndexOf(number, splitedU[0]);
                int posC = Array.IndexOf(number, splitedC[0]);
                if (posC > posU)
                    compScore++;
                else if (posU > posC)
                    userScore++;
                else
                {
                    compScore++;
                    userScore++;
                }
            }
            updateScore();
        }

        /**
         Updated the new score of the players.
         */
        private void updateScore()
        {
            label1.Text = "user: " + userScore;
            label2.Text = "computer: " + compScore;
        }


        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 239);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "user: 0";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(222, 239);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "computer: 0";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Label label1;
        private Label label2;



    }
}

